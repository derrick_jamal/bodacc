import re

import requests
from lxml import html
import csv
import pandas as pd
import re
import math

import dateparser
import datetime

base = 'https://www.bodacc.fr'
years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]
# page_list = [14, 15, 20, 21, 26, 23, 33, 31, 40, 32, 25, 30, 3]
fieldnames = ["URL",  "AD CATEGORY","BODACC", "AD NUMBER", "PUBLICATION DATE", "RCS NUMBER", "DENOMINATION", "FORM", "CAPITAL", "ADDRESS", "ZIP CODE", "LOCATION", "FROM", "START DATE OF ACTIVITY", "OPPOSITION", "COMMENTS", "ESTABLISHMENT:ORIGIN OF THE FUND", "ESTABLISHMENT:TEACHES", "ESTABLISHMENT ADDRESS", "PREVIOUS OWNER ID", "PREVIOUS OWNER NAME", "PREVIOUS OPERATOR ID", "PREVIOUS OPERATOR NAME"]
output = [] #list containing the rows extracted

for year in years:
    page_url = 'https://www.bodacc.fr/annonce/page?context=add_hit_meta%3Dnumeroparution%2540numeroparution%26add_hit_meta%3Ddatepublication%2540datepublication%26add_hit_meta%3Dnojo%2540nojo%26add_hit_meta%3Dnom%2540nom%26add_hit_meta%3Dprenom%2540prenom%26add_hit_meta%3Dtribunal%2540tribunal%26add_hit_meta%3Drm%2540rm%26add_hit_meta%3Drcs%2540rcs%26add_hit_meta%3Dnumeroannonce%2540numeroannonce%26add_hit_meta%3Dnumerodepartement%2540numerodepartement%26add_hit_meta%3Ddenomination%2540denomination%26add_hit_meta%3Dxmlbody%2540xmlbody%26s%3Ddatepublication%26q%3D%2528proc%25C3%25A9dure%2Bcollective%2529%2Bcategorieannonce_cat%253A%2528vente%2529%2B%2528%2528datepublication%253E%253D{}%2529%2B%2BAND%2B%2528datepublication%253C%253D{}%2529%2B%2529%2Bnumerodepartement%253A%252875%2529%26sa%3D0%26n%3DslBodaccDiffusion%26target%3DstBodaccDiffusion%26hf%3D10%26add_category_group%3Dpublication_cat%2540Top%252Fpublication_cat%253A1%26add_category_group%3Dcategorieannonce_cat%2540Top%252Fcategorieannonce_cat%253A1%26add_category_group%3Dtypeannonce_cat%2540Top%252Ftypeannonce_cat%253A1%26lang%3Dfr&page=1'.format(year, year+1)
    response = requests.get(page_url)
    page_content = response.content
    page_doc = html.fromstring(page_content)
    number_of_pages = page_doc.xpath('//*[@id="resultats"]/h3[@class="onglet results floatLeft"]/text()')
    number_of_pages = ''.join(number_of_pages)
    number_of_pages = int(re.search(r'\d+', number_of_pages).group())
    number_of_pages = math.ceil(number_of_pages / 10)
    print('Data from year:',year)
    print('Number of pages:', number_of_pages)
    for page in range(1, number_of_pages+1, 1):
        print('Page:', page)
        headers = {
            'Connection': 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Mobile Safari/537.36',
            'Sec-Fetch-User': '?1',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'navigate',
            'Referer': 'https://www.bodacc.fr/annonce/liste',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'If-Modified-Since': 'Thu, 30 Jan 2020 13:25:58 GMT',
        }

        url = 'https://www.bodacc.fr/annonce/page?context=add_hit_meta%3Dnumeroparution%2540numeroparution%26add_hit_meta%3Ddatepublication%2540datepublication%26add_hit_meta%3Dnojo%2540nojo%26add_hit_meta%3Dnom%2540nom%26add_hit_meta%3Dprenom%2540prenom%26add_hit_meta%3Dtribunal%2540tribunal%26add_hit_meta%3Drm%2540rm%26add_hit_meta%3Drcs%2540rcs%26add_hit_meta%3Dnumeroannonce%2540numeroannonce%26add_hit_meta%3Dnumerodepartement%2540numerodepartement%26add_hit_meta%3Ddenomination%2540denomination%26add_hit_meta%3Dxmlbody%2540xmlbody%26s%3Ddatepublication%26q%3D%2528proc%25C3%25A9dure%2Bcollective%2529%2Bcategorieannonce_cat%253A%2528vente%2529%2B%2528%2528datepublication%253E%253D{}%2529%2B%2BAND%2B%2528datepublication%253C%253D{}%2529%2B%2529%2Bnumerodepartement%253A%252875%2529%26sa%3D0%26n%3DslBodaccDiffusion%26target%3DstBodaccDiffusion%26hf%3D10%26add_category_group%3Dpublication_cat%2540Top%252Fpublication_cat%253A1%26add_category_group%3Dcategorieannonce_cat%2540Top%252Fcategorieannonce_cat%253A1%26add_category_group%3Dtypeannonce_cat%2540Top%252Ftypeannonce_cat%253A1%26lang%3Dfr&page={}'.format(year, year+1, page)

        response = requests.get(url, headers = headers)
        _txt = response.content
        doc = html.fromstring(_txt)
        container = doc.xpath("//tr[@class='pair' or @class='impair']")
        for listing in container:
            # looping through each data in a page
            publication_date = listing.xpath(".//td[@class='colonne1']/text()")
            publication_date = [item.replace("  ", "") for item in publication_date]
            publication_date = [item.replace("\n", "") for item in publication_date]
            publication_date = [item.replace("\t", "") for item in publication_date]
            publication_date = ''.join(publication_date)
            denomination = listing.xpath('.//dt[text()="Dénomination sociale :" or text()="Nom, prénom :"]/following-sibling::dd[1]/text()')
            denomination = [item.replace("  ", "") for item in denomination]
            denomination = [item.replace("\t", "") for item in denomination]
            denomination = [item.replace("\n", "") for item in denomination]
            denomination = ''.join(denomination)
            # name = listing.xpath('.//dt[]/following-sibling::dd[1]/text()')
            # name = [item.replace("  ", "") for item in name]
            # name = [item.replace("\t", "") for item in name]
            # name = [item.replace("\n", "") for item in name]
            # name = ''.join(name)
            url = listing.xpath(".//p/a/@href")
            # rcs_number = container.xpath('.//dt/abbr[contains(@title, "Registre du commerce et des sociétés")]/following-sibling::dd[1]/text()')
            ad_category = listing.xpath('.//dt[contains(text(), "Catégorie d\'annonce :")]/following-sibling::dd[1]/text()')
            ad_category = [item.replace("  ", "") for item in ad_category]
            ad_category = [item.replace("\t", "") for item in ad_category]
            ad_category = [item.replace("\n", "") for item in ad_category]
            ad_category = ''.join(ad_category)
            # department = listing.xpath('.//dt[contains(text(), "Département :")]/following-sibling::dd[1]/text()')
            # department = [item.replace("  ", "") for item in department]
            # department = [item.replace("\t", "") for item in department]
            # department = [item.replace("\n", "") for item in department]
            # department = ''.join(department)
            # ad_posted_to = listing.xpath('.//dt[contains(text(), "Annonce déposée au :")]/following-sibling::dd[1]/text()')
            # ad_posted_to = [item.replace("  ", "") for item in ad_posted_to]
            # ad_posted_to = [item.replace("\t", "") for item in ad_posted_to]
            # ad_posted_to = [item.replace("\n", "") for item in ad_posted_to]
            # ad_posted_to = ''.join(ad_posted_to)
            # publication_references = listing.xpath('.//dt[contains(text(), "Références de publication :")]/following-sibling::dd[1]/text()')
            # publication_references = [item.replace("  ", "") for item in publication_references]
            # publication_references = [item.replace("\t", "") for item in publication_references]
            # publication_references = [item.replace("\n", "") for item in publication_references]
            # publication_references = ''.join(publication_references)

            # get the detail page
            str_url = ''.join(url)
            detail_url = base + str_url
            # print('Detail_url:', detail_url)
            detail_response = requests.get(detail_url)
            text = detail_response.content
            details = html.fromstring(text)
            url_split = detail_url.split('/')
            bodacc = url_split[5]
            ad_number = url_split[6]
            rcs_number = details.xpath('//*[@id="annonce"]/dl/dt/abbr[@title="Registre du commerce et des sociétés"]/following::dd[1]/text()')
            rcs_number = [item.replace("  ", "") for item in rcs_number]
            rcs_number = [item.replace("\t", "") for item in rcs_number]
            rcs_number = [item.replace("\n", "") for item in rcs_number]
            rcs_number = ''.join(rcs_number)
            rcs_number = re.findall(r'\d+', rcs_number)
            rcs_number = ''.join(rcs_number)
            rcs_number = ' '.join([rcs_number[i:i+3] for i in range(0, len(rcs_number), 3)])
            form = details.xpath('.//*[@id="annonce"]/dl/dt[text()="Forme :" or text()="Forme : "]/following-sibling::dd[1]/text()')
            form = [item.replace("  ", "") for item in form]
            form = [item.replace("\t", "") for item in form]
            form = [item.replace("\n", "") for item in form]
            form = ''.join(form)
            capital = details.xpath('.//*[@id="annonce"]/dl/dt[text()="Capital :"]/following-sibling::dd[1]/text()')
            capital = [item.replace("  ", "") for item in capital]
            capital = [item.replace("\t", "") for item in capital]
            capital = [item.replace("\n", "") for item in capital]
            capital = ''.join(capital)
            capital = re.findall(r'\d+', capital)
            capital = ''.join(capital)
            address_code = details.xpath('.//*[@id="annonce"]/dl/dt[text()="Adresse :" or normalize-space()="Adresse :"]/following-sibling::dd[1]/text()') or None
            if address_code is not None:
                address_code = [item.replace("  ", "") for item in address_code]
                address_code = [item.replace("\t", "") for item in address_code]
                address_code = [item.replace("\n", "") for item in address_code]
                address_code = ''.join(address_code)
                split = [x.strip() for x in re.split("(\d{5})", address_code)]
                # print('split address is', split)
                # print('length of split address', len(split))
                if len(split) > 1:
                    # method 1 : assuming the address code are 5 numbers
                    address_split = [x.strip() for x in re.split("(\d{5})", address_code)]
                    address = address_split[0]
                    address = ''.join(address)
                    zip_code = address_split[1]
                    zip_code = ''.join(zip_code)
                    location = address_split[2]
                    location = ''.join(location)
                else:
                    address = ''
                    zip_code = ''
                    location = address_code
                    location = ''.join(location)
            else:
                address = ''
                zip_code = ''
                location = ''
                #method 2 : assuming address starts with the street number and the next number will be the code
            # parts = address_code.split()
            # for i in range(len(parts) -1, 0, 1):
            #     if parts[i].isdigit():
            #         address_split = ' '.join(parts[:i]), parts[i], ' '.join(parts[i+1:])
            #         address = address_split[0]
            #         zip_code = address_split[1]
            #         location = address_split[2]
            date_from = details.xpath('//*[@id="annonce"]/dl/dt[text()="A dater du :"]/following-sibling::dd[1]/text()') or None
            if date_from is not None:
                date_from = [item.replace("  ", "") for item in date_from]
                date_from = [item.replace("\t", "") for item in date_from]
                date_from = [item.replace("\n", "") for item in date_from]
                date_from = ' '.join(date_from)
                date_from = dateparser.parse(date_from)
                date_from = date_from.strftime("%d/%m/%Y")
            else:
                date_from = ''
            start_date_of_activity = details.xpath('//*[@id="annonce"]/dl/dt[text()="Date de commencement d\'activité :"]/following-sibling::dd[1]/text()') or None
            if start_date_of_activity is not None:
                start_date_of_activity = [item.replace("  ", "") for item in start_date_of_activity]
                start_date_of_activity = [item.replace("\t", "") for item in start_date_of_activity]
                start_date_of_activity = [item.replace("\n", "") for item in start_date_of_activity]
                start_date_of_activity = ' '.join(start_date_of_activity)
                start_date_of_activity = dateparser.parse(start_date_of_activity)
                start_date_of_activity = start_date_of_activity.strftime("%d/%m/%Y")
            else:
                start_date_of_activity = ''
            establishment = details.xpath('//*[@id="annonce"]/dl/dt[text()="Etablissement(s) :"]/following-sibling::dd/dl/dt[text()="Origine du fond :"]/following-sibling::dd[1]/text()')
            establishment = [item.replace("  ", "") for item in establishment]
            establishment = [item.replace("\t", "") for item in establishment]
            establishment = [item.replace("\n", "") for item in establishment]
            establishment = ' '.join(establishment)
            establishment_teaches = details.xpath('//*[@id="annonce"]/dl/dt[text()[normalize-space()="Etablissement(s) :"]]/following-sibling::dd/dl/dt[text()="Enseigne :"]/following-sibling::dd[1]/text()')
            establishment_teaches = [item.replace("  ", "") for item in establishment_teaches]
            establishment_teaches = [item.replace("\t", "") for item in establishment_teaches]
            establishment_teaches = [item.replace("\n", "") for item in establishment_teaches]
            establishment_teaches = ' '.join(establishment_teaches)
            establishment_address = details.xpath('//*[@id="annonce"]/dl/dt[text()="Etablissement(s) :"]/following-sibling::dd/dl/dt[text()="Adresse de l\'établissement :"]/following-sibling::dd[1]/text()')
            establishment_address = [item.replace("  ", "") for item in establishment_address]
            establishment_address = [item.replace("\t", "") for item in establishment_address]
            establishment_address = [item.replace("\n", "") for item in establishment_address]
            establishment_address = ' '.join(establishment_address)
            previous_owner_id = details.xpath('(//*[@id="annonce"]//dt[text()="Précédent(s) Propriétaire(s) :"]/following-sibling::dd//dt[text()[normalize-space()="n°Identification :"]]/following-sibling::dd[1]/text())[1]')
            owner_id = [item.replace("  ", "") for item in previous_owner_id]
            owner_id = [item.replace("\t", "") for item in owner_id]
            owner_id = [item.replace("\n", "") for item in owner_id]
            owner_id = ' '.join(owner_id)
            # owner_id = owner_id.split(",")
            # owner_id = owner_id[0]
            # previous_owner_id = previous_owner_id.partition(' ')[0]
            previous_owner_name = details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) Propriétaire(s) :"]/following-sibling::dd/dl/dt[text()="Dénomination :"]/following-sibling::dd[1]/text())[1]') or (details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) Propriétaire(s) :"]/following-sibling::dd/dl/dt[text()="Nom :"]/following-sibling::dd[1]/text())[1]') + details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) Propriétaire(s) :"]/following-sibling::dd/dl/dt[text()="Prénom :"]/following-sibling::dd[1]/text())[1]'))
            previous_owner_name = [item.replace("  ", "") for item in previous_owner_name]
            previous_owner_name = [item.replace("\t", "") for item in previous_owner_name]
            previous_owner_name = [item.replace("\n", "") for item in previous_owner_name]
            previous_owner_name = ','.join(previous_owner_name)
            # previous_owner_name = previous_owner_name.split(",")
            # previous_owner_name = previous_owner_name[0]
            previous_operators_id = details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) exploitant(s) :"]/following-sibling::dd/dl/dt[text()[normalize-space()="n°Identification :"]]/following-sibling::dd[1]/text())[1]')
            previous_operators_id = [item.replace("  ", "") for item in previous_operators_id]
            previous_operators_id = [item.replace("\t", "") for item in previous_operators_id]
            previous_operators_id = [item.replace("\n", "") for item in previous_operators_id]
            previous_operators_id = ''.join(previous_operators_id)
            previous_operators_name = details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) exploitant(s) :"]/following-sibling::dd/dl/dt[text()="Dénomination :"]/following-sibling::dd[1]/text())[1]') or (details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) exploitant(s) :"]/following-sibling::dd/dl/dt[text()="Nom :"]/following-sibling::dd[1]/text())[1]') + details.xpath('(//*[@id="annonce"]/dl/dt[text()="Précédent(s) exploitant(s) :"]/following-sibling::dd/dl/dt[text()="Prénom :"]/following-sibling::dd[1]/text())[1]'))
            previous_operators_name = [item.replace("  ", "") for item in previous_operators_name]
            previous_operators_name = [item.replace("\t", "") for item in previous_operators_name]
            previous_operators_name = [item.replace("\n", "") for item in previous_operators_name]
            previous_operators_name = ','.join(previous_operators_name)
            oppositions = details.xpath('//*[@id="annonce"]/dl/dt[text()="Oppositions :"]/following-sibling::dd[1]/text()')
            oppositions = [item.replace("  ", "") for item in oppositions]
            oppositions = [item.replace("\t", "") for item in oppositions]
            oppositions = [item.replace("\n", "") for item in oppositions]
            oppositions = ''.join(oppositions)
            comments = details.xpath('//*[@id="annonce"]/dl/dt[text()="Commentaires :"]/following-sibling::dd[1]/text()')
            comments = [item.replace("  ", "") for item in comments]
            comments = [item.replace("\t", "") for item in comments]
            comments = [item.replace("\n", "") for item in comments]
            comments = ''.join(comments)
            values = [detail_url, ad_category, bodacc, ad_number, publication_date, rcs_number, denomination, form, capital, address, zip_code, location, date_from, start_date_of_activity, oppositions, comments, establishment, establishment_teaches, establishment_address, owner_id, previous_owner_name, previous_operators_id, previous_operators_name]
            resp = dict(zip(fieldnames, values))
            print(resp)
            output.append(resp) #appending each object to list
        print("The output is:",output)
        print(len(output))
        #csv file output
        with open("bodacc.csv", "w", newline='') as f:
            # sort = sorted(f, key=operator.itemgetter(0))
            writer = csv.DictWriter(f, fieldnames = fieldnames, lineterminator = '\n')
            writer.writeheader()
            for row in output:
                writer.writerow(row)
         #using pandas to split by year
        df = pd.read_csv('bodacc.csv')
        cols = df.columns

df['Year'] = df['PUBLICATION DATE'].apply(lambda x: x.split('/')[-1])

for i in set(df.Year): # for classified by years files
    filename = "byYear/"+i+".csv"
    df.loc[df.Year == i].to_csv(filename,index=False,columns=cols)
